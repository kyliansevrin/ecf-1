// Recupération des données de l'api
Promise.all([
    window.fetch("../json/tarif-film.json")
    .then ( function (response) {
        return response.json();
    })
    .then(function (data){
        for (const element in data) {
            window.fetch('http://www.omdbapi.com/?apikey=aa873ea5&i='+data[element].id)
            .then ( function (response) {
                return response.json();
            })
            .then(function (film) {
                displayMovies(film);
                affiche = document.getElementById('film'+film.imdbID)
                affiche.addEventListener('click', function () {
                    displayInfo(film);
                });
            })
            .then ( () => {
                let event = new Event("clickaffiche");
                window.dispatchEvent(event);
            })
        }
    })
]);

function displayInfo(JsonObject){
    let bloc_film = document.getElementById('bloc-film');
    let localistion = document.getElementById('localisation');

    const Poster = JsonObject.Poster;
    const Title = JsonObject.Title;
    const Plot = JsonObject.Plot;

    //On affiche l'image 
    document.getElementById('affiche-film').src=Poster;

    //On affiche le titre
    document.getElementById('titre-film').innerHTML=Title;

    //On affiche la description du film 
    document.getElementById('information').innerHTML=Plot;

    //On fait apparaître le bloc pour le film et on enleve celui de base 
    bloc_film.style.display='flex';
    localistion.style.display='none';
}

function displayMovies(JsonObject) {
    const Poster = JsonObject.Poster;
    const Title = JsonObject.Title;
    const Note = JsonObject.Ratings[0].Value;
    const Sortie = JsonObject.Released;

    let div1 = document.createElement('div');
    div1.setAttribute('class', 'list-films');

    let parent = document.getElementById('list-film');
    parent.append(div1);

    if (window.location.href.split('/').pop()=="index.html") {
        let img = document.createElement('img');
        img.src = Poster;
        img.id = 'film'+JsonObject.imdbID;
        img.classList.add('film');
        div1.append(img);
    }
    else{
        let a = document.createElement('a');
        a.href = 'index.html';
        div1.append(a);

        let img = document.createElement('img');
        img.src = Poster;
        img.id = 'film'+JsonObject.imdbID;
        img.classList.add('film');
        a.append(img);
    }

    let div2 = document.createElement('div');
    div2.classList.add("film-desc");
    div1.append(div2);

    let titre = document.createElement('h3');
    titre.innerHTML=Title;
    titre.id = 'titre-film'+JsonObject.imdbID
    div2.append(titre);

    let pDiv = document.createElement('div');
    let note = document.createElement('p');
    note.innerHTML=Note;
    note.id = 'note'+JsonObject.imdbID;
    pDiv.append(note);
    let psortie = document.createElement('p')
    psortie.innerHTML=Sortie;
    psortie.id = 'sortie'+JsonObject.imdbID;

    pDiv.append(psortie);
    div2.append(pDiv);
}

//Au clique sur le bouton en savoir+, on réaffiche le bloc d'information sur le cinema dans l'accueil
let btn = document.getElementById('savoir+');
let film = document.getElementById('bloc-film');
let localisation = document.getElementById('localisation');
btn.addEventListener('click', function() {
    if (btn.classList.toggle){
        film.style.display='none';
        localisation.style.display='flex';
    }
})



