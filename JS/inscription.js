// Variable pour les boutons créer un compte/se connecter 
let btn2 = document.querySelector("#connecte");
let btn1 = document.querySelector('#creer-compte');

//On affiche le block de connexion si le bouton est cliqué 
btn2.addEventListener('click', function() {
    if (btn2.classList.toggle){
        document.querySelector('.container').style.display='none';
        document.querySelector('.container1').style.display='block';
    }
})

//On affiche le block d'inscription si le bouton est cliqué 
btn1.addEventListener('click', function() {
    if (btn1.classList.toggle){
        document.querySelector('.container').style.display='block';
        document.querySelector('.container1').style.display='none';
    }
})

//je n'ai pas su comment limiter l'âge pour la date.
function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

let btncreer= document.getElementById('creer');
let form= document.getElementById('login')

form.addEventListener('submit', function (event) {
    let date = new Date(document.getElementById('date').value);
    if (getAge(date) < 18){
        window.alert("Vous n'avez pas 18 ans");
        event.preventDefault();
    }
})

