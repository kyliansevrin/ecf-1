# Front-end Project

Ce project avait pour but de nous faire appliquer ce qu'on avait appris en Front-End.
Il était demandé de créer un site internet pour un cinema, et pour ceci
nous avions 3 jours pour créer le tout.

## Fonctionnalités demandées

* -Créez un fichier HTML de la page d’accueil du site en respectant la charte graphique avec une 
photo du cinéma, son adresse… Prévoir un dégradé des deux couleurs principales par-dessus 
l’image du film de la semaine.

- Créer un fichier HTML avec les différents films associés aux horaires et tarifs. Un bouton 
« réserver » permet, au clic d’afficher sur la droite de cette section un « aside » pour la 
réservation de tickets de ce film

-> Créez un « header » contenant une barre de navigation avec les onglets :
- Home
- Tarifs
- Panier
- Inscription Newsletter

La page sur laquelle on se trouve aura son titre en rouge. Le logo se trouvera avant cette navbar et renverra systématiquement sur la page d’accueil. Après cette nav-bar, on trouvera une 
icône permettant de se connecter / s’inscrire

-> Créez un « footer » avec :
- Trois liens vers des réseaux sociaux
- Une barre de recherche
- Un bouton se connecter

-> Créer un fichier HTML d’inscription. Ce sera un formulaire vérifiant les données en HTML puis 
certaines en Javascript. Le formulaire ne sera envoyé que si la totalité des données saisies 
sont correctes.
    A vérifier : 
    Toutes les données sont obligatoires
    - Civilité : Utilisez un select
    - Nom : La première lettre est une majuscule (en JavaScript), la taille est de 40 caractères 
    maximums
    - Prénom : la taille est de 2 caractères minimum et 40 maximum
    - Adresse (numéro de rue, rue, code postal, ville) : ne peuvent être vides
    - Date de naissance : la personne doit être majeur au jour de l’inscription (en
    - JavaScript)
    - Adresse mail : vérifier qu’il s’agit d’une adresse valide

-> Un fichier JSON donnera la liste des films pouvant être programmés par le cinéma de quartier :
- Les films, les séances et les tarifs seront entrées manuellement dans un fichier JSON avec 
l’identifiant du film associé à un label contenant la date et l’heure prévue ainsi que le tarif de 
celle-ci. Optionnellement il est possible de prévoir un programme sur une semaine complète 
ainsi que plusieurs tarifs en fonction de l’heure ou de l’âge

- Un appel à l’API OMDb http://www.omdbapi.com/ sera effectué pour récupérer les caractéristiques 
des films (affiches, titres, notes, description du film, acteurs, réalisateurs, notes…) aussi bien sur la 
page d’accueil que sur la page de description du film.

### `index.html`

* La page index.html est en grande partie terminer, elle permet d'afficher les films
de la semaine ainsi en cliquant sur une des affiches, les informations du film
en question s'afficheront.

### `inscription.html`

* La page inscription.html permet d'afficher des formulaires sois pour la
création sois pour la connexion à son compte client.

La vérification pour l'âge a été effectué en JavaScript.

### `panier.html`

*La page panier.html n'as pas encore été fait par manque de temps.

### `tarifs.html`

*La page tarif.html n'est pas terminé non plus, mais elle est encore en construction
à ce jour.

## Setup

Vous pouvez lancer le projet avec un serveur local :

Si vous avez PHP installé et accessible en ligne de commande, vous pouvez lancer un serveur avec la commande suivante :

```bash
php -S localhost:8000 -t ./src/
```

Vous pouvez visiter le "site" à l'adresse http://localhost:8000/index.html

Ouvrir les fichiers directement ne fonctionnera pas : quand le fichier est 
ouvert en mode "file:///path/to/file.html" JavaScript n'est pas autorisé 
à faire des appels.
